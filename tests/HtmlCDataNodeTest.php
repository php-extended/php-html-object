<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Html\HtmlCDataNode;
use PHPUnit\Framework\TestCase;

/**
 * HtmlCDataNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Html\HtmlCDataNode
 *
 * @internal
 *
 * @small
 */
class HtmlCDataNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var HtmlCDataNode
	 */
	protected HtmlCDataNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('<![CDATA[value]]>', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new HtmlCDataNode('value');
	}
	
}
