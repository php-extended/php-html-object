<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Html\HtmlCollectionNode;
use PHPUnit\Framework\TestCase;

/**
 * HtmlCollectionNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Html\HtmlCollectionNode
 *
 * @internal
 *
 * @small
 */
class HtmlCollectionNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var HtmlCollectionNode
	 */
	protected HtmlCollectionNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('<name></name>', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new HtmlCollectionNode('name');
	}
	
}
