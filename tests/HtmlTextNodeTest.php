<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Html\HtmlTextNode;
use PHPUnit\Framework\TestCase;

/**
 * HtmlTextNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Html\HtmlTextNode
 *
 * @internal
 *
 * @small
 */
class HtmlTextNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var HtmlTextNode
	 */
	protected HtmlTextNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('value', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new HtmlTextNode('value');
	}
	
}
