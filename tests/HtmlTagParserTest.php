<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Html\HtmlCollectionNode;
use PhpExtended\Html\HtmlSingleNode;
use PhpExtended\Html\HtmlTagParser;
use PHPUnit\Framework\TestCase;

/**
 * HtmlTagParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Html\HtmlTagParser
 *
 * @internal
 *
 * @small
 */
class HtmlTagParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var HtmlTagParser
	 */
	protected HtmlTagParser $_parser;
	
	public function testEmpty() : void
	{
		$this->assertEquals(new HtmlCollectionNode('span'), $this->_parser->parse(null));
	}
	
	public function testSimpleTag() : void
	{
		$this->assertEquals(new HtmlCollectionNode('a'), $this->_parser->parse('<a>'));
	}
	
	public function testAutoclosedTag() : void
	{
		$this->assertEquals(new HtmlSingleNode('hr', [], '', true), $this->_parser->parse('<hr>'));
	}
	
	public function testNamespacedTag() : void
	{
		$this->assertEquals(new HtmlCollectionNode('ns:a'), $this->_parser->parse('<ns:a>'));
	}
	
	public function testAttribute() : void
	{
		$this->assertEquals(new HtmlCollectionNode('a', ['href' => 'toto']), $this->_parser->parse('<a href=toto>'));
	}
	
	public function testQuotedAttribute() : void
	{
		$this->assertEquals(new HtmlCollectionNode('a', ['href' => 'toto']), $this->_parser->parse('<a href="toto">'));
	}
	
	public function testSQuotedAttribute() : void
	{
		$this->assertEquals(new HtmlCollectionNode('a', ['href' => 'toto']), $this->_parser->parse("<a href='toto'>"));
	}
	
	public function testNamespacedAttribute() : void
	{
		$this->assertEquals(new HtmlCollectionNode('a', ['ns:href' => 'toto']), $this->_parser->parse('<a ns:href="toto">'));
	}
	
	public function testSingleAttribute() : void
	{
		$this->assertEquals(new HtmlCollectionNode('a', ['required' => 'required']), $this->_parser->parse('<a required>'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new HtmlTagParser();
	}
	
}
