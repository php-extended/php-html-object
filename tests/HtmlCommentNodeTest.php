<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Html\HtmlCommentNode;
use PHPUnit\Framework\TestCase;

/**
 * HtmlCommentNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Html\HtmlCommentNode
 *
 * @internal
 *
 * @small
 */
class HtmlCommentNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var HtmlCommentNode
	 */
	protected HtmlCommentNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('<!--text-->', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new HtmlCommentNode('text');
	}
	
}
