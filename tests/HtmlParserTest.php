<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Html\HtmlCollectionNode;
use PhpExtended\Html\HtmlCommentNode;
use PhpExtended\Html\HtmlDoctypeNode;
use PhpExtended\Html\HtmlParser;
use PhpExtended\Html\HtmlTextNode;
use PHPUnit\Framework\TestCase;

/**
 * HtmlParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Html\HtmlParser
 *
 * @internal
 *
 * @small
 */
class HtmlParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var HtmlParser
	 */
	protected HtmlParser $_parser;
	
	public function testEmpty() : void
	{
		$this->assertEquals(new HtmlCollectionNode('document'), $this->_parser->parse(null));
	}
	
	public function testText() : void
	{
		$this->assertEquals(new HtmlCollectionNode('document', [], [
			new HtmlTextNode('toto'),
		]), $this->_parser->parse('toto'));
	}
	
	public function testDocument() : void
	{
		$this->assertEquals(new HtmlCollectionNode('document', [], [
			new HtmlCollectionNode('html', [], [
				new HtmlCollectionNode('body', ['class' => 'htmlpage'], [
					new HtmlTextNode(' hello! '),
				]),
			]),
		]), $this->_parser->parse('<html><body class="htmlpage"> hello! </body></html>'));
	}
	
	public function testInvalidDocument() : void
	{
		$this->assertEquals(new HtmlCollectionNode('document', [], [
			new HtmlCollectionNode('html', [], [
				new HtmlCollectionNode('body', ['class' => 'htmlpage'], [
					new HtmlTextNode(' hello! '),
				]),
			]),
		]), $this->_parser->parse('<html><body class="htmlpage"> hello! </html>'));
	}
	
	public function testValidComment() : void
	{
		$this->assertEquals(new HtmlCollectionNode('document', [], [
			new HtmlCollectionNode('html', [], [
				new HtmlCollectionNode('body', [], [
					new HtmlCommentNode(' comment '),
				]),
			]),
		]), $this->_parser->parse('<html><body><!-- comment --></body></html>'));
	}
	
	public function testInvalidComment() : void
	{
		$this->assertEquals(new HtmlCollectionNode('document', [], [
			new HtmlCollectionNode('html', [], [
				new HtmlCollectionNode('body', [], [
					new HtmlTextNode('<!- data -->'),
				]),
			]),
		]), $this->_parser->parse('<html><body><!- data --></body></html>'));
	}
	
	public function testDoctype() : void
	{
		$this->assertEquals(new HtmlCollectionNode('document', [], [
			new HtmlDoctypeNode('DOCTYPE html'),
		]), $this->_parser->parse('<!DOCTYPE html>'));
	}
	
	public function testParseNoHtml() : void
	{
		$expected = new HtmlCollectionNode('document', [], [
			new HtmlTextNode('non spécifié'),
		]);
		$this->assertEquals($expected, $this->_parser->parse('non spécifié'));
		$this->assertEquals('non spécifié', $expected->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new HtmlParser();
	}
	
}
