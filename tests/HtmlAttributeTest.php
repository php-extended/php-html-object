<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Html\HtmlAttribute;
use PHPUnit\Framework\TestCase;

/**
 * HtmlAttributeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Html\HtmlAttribute
 *
 * @internal
 *
 * @small
 */
class HtmlAttributeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var HtmlAttribute
	 */
	protected HtmlAttribute $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('name="value"', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new HtmlAttribute('name', 'value');
	}
	
}
