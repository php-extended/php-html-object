<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Html\HtmlSingleNode;
use PHPUnit\Framework\TestCase;

/**
 * HtmlSingleNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Html\HtmlSingleNode
 *
 * @internal
 *
 * @small
 */
class HtmlSingleNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var HtmlSingleNode
	 */
	protected HtmlSingleNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('<name></name>', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new HtmlSingleNode('name');
	}
	
}
