<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

/**
 * HtmlTransformerCommentFilter class file.
 * 
 * This class is a filter for html trees that removes comment nodes from the
 * tree.
 * 
 * @author Anastaszor
 */
class HtmlTransformerCommentFilter implements HtmlTransformerInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlTransformerInterface::transform()
	 */
	public function transform(HtmlCollectionNodeInterface $dom) : HtmlCollectionNodeInterface
	{
		$children = [];
		
		foreach($dom as $child)
		{
			if(HtmlAbstractNodeInterface::TYPE_COMMENT === $child->getName())
			{
				continue;
			}
			
			if($child instanceof HtmlCollectionNodeInterface)
			{
				$child = $this->transform($child);
			}
			
			$children[] = $child;
		}
		
		$attributes = [];
		
		foreach($dom->getAttributes() as $attribute)
		{
			$attributes[] = $attribute;
		}
		
		return new HtmlCollectionNode($dom->getName(), $attributes, $children);
	}
	
}
