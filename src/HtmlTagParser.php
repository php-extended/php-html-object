<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;

/**
 * HtmlTagParser class file.
 * 
 * This parser parses only the begin tag of an html node.
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<HtmlAbstractNodeInterface>
 */
class HtmlTagParser extends AbstractParserLexer
{
	
	public const L_CHR_ALPHANUM = 1; // al and -
	public const L_CHR_WHITESPAC = 2; // sp
	public const L_CHR_START_TAG = 3; // <
	public const L_CHR_END_TAG = 4; // >
	public const L_CHR_COLON = 5; // :
	public const L_CHR_EQUAL_TAG = 6; // =
	public const L_CHR_DQUOT_TAG = 7; // "
	public const L_CHR_SQUOT_TAG = 8; // '
	public const L_CHR_ENDIN_TAG = 9; // /
	public const L_CHR_ESCAP_TAG = 10; // \
	public const L_ATT_WQUOT_VAL = 11; // =**
	public const L_ATT_DQUOT_VAL = 12; // ="**
	public const L_ATT_DQUOT_ESC = 13; // ="**\
	public const L_ATT_DQUOT_CMP = 14; // ="**"
	public const L_ATT_SQUOT_VAL = 15; // ='**
	public const L_ATT_SQUOT_ESC = 16; // ='**\
	public const L_ATT_SQUOT_CMP = 17; // ='**"
	public const L_ATT_NAMESPACE = 18; // **:
	public const L_ATT_NAMED_TAG = 19; // **:**
	
	// use isset instead of in_array, performance boost about 30%...
	/**
	 * @var array<string, int>
	 */
	protected array $_autoclosedTags = [
		'img' => 1, 'br' => 1, 'wbr' => 1, 'input' => 1, 'meta' => 1, 'link' => 1, 'hr' => 1, 'base' => 1, 'embed' => 1, 'spacer' => 1,
	];
	
	/**
	 * @var array<string, int>
	 */
	protected array $_blockTags = [
		'root' => 1, 'body' => 1, 'form' => 1, 'div' => 1, 'span' => 1, 'table' => 1,
	];
	
	/**
	 * @var array<string, array<string, int>>
	 */
	protected array $_optionalClosingTags = [
		'tr' => ['tr' => 1, 'td' => 1, 'th' => 1],
		'th' => ['th' => 1],
		'td' => ['td' => 1],
		'li' => ['li' => 1],
		'dt' => ['dt' => 1, 'dd' => 1],
		'dd' => ['dd' => 1, 'dt' => 1],
		'dl' => ['dd' => 1, 'dt' => 1],
		'p' => ['p' => 1],
		'nobr' => ['nobr' => 1],
	];
	
	/**
	 * Builds a new HtmlTagParser with the given lexer configuration.
	 */
	public function __construct()
	{
		parent::__construct(HtmlAbstractNodeInterface::class);
		$this->_config->addMappings('<', self::L_CHR_START_TAG);
		$this->_config->addMappings('>', self::L_CHR_END_TAG);
		$this->_config->addMappings('-', self::L_CHR_ALPHANUM);
		$this->_config->addMappings(':', self::L_CHR_COLON);
		$this->_config->addMappings('=', self::L_CHR_EQUAL_TAG);
		$this->_config->addMappings('"', self::L_CHR_DQUOT_TAG);
		$this->_config->addMappings("'", self::L_CHR_SQUOT_TAG);
		$this->_config->addMappings('\\', self::L_CHR_ESCAP_TAG);
		$this->_config->addMappings('/', self::L_CHR_ENDIN_TAG);
		$this->_config->addMappings(LexerInterface::CLASS_SPACE, self::L_CHR_WHITESPAC);
		$this->_config->addMappings(LexerInterface::CLASS_ALNUM, self::L_CHR_ALPHANUM);
		
		$this->_config->addMerging(self::L_CHR_WHITESPAC, self::L_CHR_WHITESPAC, self::L_CHR_WHITESPAC);
		$this->_config->addMerging(self::L_CHR_ALPHANUM, self::L_CHR_ALPHANUM, self::L_CHR_ALPHANUM);
		
		$this->_config->addMerging(self::L_CHR_ALPHANUM, self::L_CHR_EQUAL_TAG, self::L_ATT_WQUOT_VAL);
		$this->_config->addMerging(self::L_ATT_WQUOT_VAL, self::L_CHR_ALPHANUM, self::L_ATT_WQUOT_VAL);
		
		$this->_config->addMerging(self::L_ATT_WQUOT_VAL, self::L_CHR_DQUOT_TAG, self::L_ATT_DQUOT_VAL);
		$this->_config->addMerging(self::L_ATT_DQUOT_VAL, self::L_CHR_DQUOT_TAG, self::L_ATT_DQUOT_CMP);
		$this->_config->addMerging(self::L_ATT_DQUOT_VAL, self::L_CHR_ESCAP_TAG, self::L_ATT_DQUOT_ESC);
		$this->_config->addMerging(self::L_ATT_DQUOT_VAL, LexerInterface::L_EVERYTHING_ELSE, self::L_ATT_DQUOT_VAL);
		$this->_config->addMerging(self::L_ATT_DQUOT_ESC, LexerInterface::L_EVERYTHING_ELSE, self::L_ATT_DQUOT_VAL);
		
		$this->_config->addMerging(self::L_ATT_WQUOT_VAL, self::L_CHR_SQUOT_TAG, self::L_ATT_SQUOT_VAL);
		$this->_config->addMerging(self::L_ATT_SQUOT_VAL, self::L_CHR_SQUOT_TAG, self::L_ATT_SQUOT_CMP);
		$this->_config->addMerging(self::L_ATT_SQUOT_VAL, self::L_CHR_ESCAP_TAG, self::L_ATT_SQUOT_ESC);
		$this->_config->addMerging(self::L_ATT_SQUOT_VAL, LexerInterface::L_EVERYTHING_ELSE, self::L_ATT_SQUOT_VAL);
		$this->_config->addMerging(self::L_ATT_SQUOT_ESC, LexerInterface::L_EVERYTHING_ELSE, self::L_ATT_SQUOT_VAL);
		
		$this->_config->addMerging(self::L_CHR_ALPHANUM, self::L_CHR_COLON, self::L_ATT_NAMESPACE);
		$this->_config->addMerging(self::L_ATT_NAMESPACE, self::L_CHR_ALPHANUM, self::L_ATT_NAMED_TAG);
		$this->_config->addMerging(self::L_ATT_NAMED_TAG, self::L_CHR_ALPHANUM, self::L_ATT_NAMED_TAG);
		$this->_config->addMerging(self::L_ATT_NAMED_TAG, self::L_CHR_EQUAL_TAG, self::L_ATT_WQUOT_VAL);
	}
	
	/**
	 * Parses the data that comes from the given lexer.
	 * 
	 * @param LexerInterface $lexer
	 * @return HtmlAbstractNodeInterface
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function parseLexer(LexerInterface $lexer) : HtmlAbstractNodeInterface
	{
		$started = false;
		$name = null;
		$attr = [];
		$lexer->rewind();
		
		while($lexer->valid())
		{
			/** @var \PhpExtended\Lexer\Lexeme $lexeme */
			$lexeme = $lexer->current();
			if(!$started)
			{
				$started = $lexeme->getCode() === self::L_CHR_START_TAG;
				// just ignore everything before the first start tag
				$lexer->next();
				continue;
			}
			
			switch($lexeme->getCode())
			{
				case self::L_CHR_END_TAG:
					// end of tag, even if end of stream
					break 2;
				
				case self::L_CHR_ALPHANUM:
				case self::L_ATT_NAMESPACE:
				case self::L_ATT_NAMED_TAG:
					$value = $this->decode($lexeme->getData());
					$value = \trim($value, ':'); // un-namespace
					if(null === $name)
					{
						$name = $value;
						break;
					}
					
					$attr[$value] = $value;
					break;
				
				case self::L_ATT_WQUOT_VAL:
					$value = \explode('=', $lexeme->getData(), 2);
					$attname = $this->decode($value[0]);
					if(isset($value[1]))
					{
						$attval = $this->decode($value[1]);
						$attr[$attname] = $attval;
					}
					break;
				
				case self::L_ATT_DQUOT_VAL:
				case self::L_ATT_DQUOT_ESC:
				case self::L_ATT_DQUOT_CMP:
					$value = \explode('=', $lexeme->getData(), 2);
					$attname = $this->decode($value[0]);
					if(isset($value[1]))
					{
						$attval = $this->decode(\str_replace('\\"', '"', $value[1]));
						if(0 < (int) \mb_strlen($attval) && '"' === $attval[0])
						{
							$attval = (string) \mb_substr($attval, 1);
						}
						if(0 < (int) \mb_strlen($attval) && '"' === $attval[(int) \mb_strlen($attval) - 1])
						{
							$attval = (string) \mb_substr($attval, 0, -1);
						}
						$attr[$attname] = $attval;
					}
					break;
				
				case self::L_ATT_SQUOT_VAL:
				case self::L_ATT_SQUOT_ESC:
				case self::L_ATT_SQUOT_CMP:
					$value = \explode('=', $lexeme->getData(), 2);
					$attname = $this->decode($value[0]);
					if(isset($value[1]))
					{
						$attval = $this->decode(\str_replace("\\'", "'", $value[1]));
						if(0 < (int) \mb_strlen($attval) && "'" === $attval[0])
						{
							$attval = (string) \mb_substr($attval, 1);
						}
						if(0 < (int) \mb_strlen($attval) && "'" === $attval[(int) \mb_strlen($attval) - 1])
						{
							$attval = (string) \mb_substr($attval, 0, -1);
						}
						$attr[$attname] = $attval;
					}
					break;
				
				case self::L_CHR_START_TAG:
				case self::L_CHR_EQUAL_TAG:
				case self::L_CHR_DQUOT_TAG:
				case self::L_CHR_SQUOT_TAG:
				case self::L_CHR_ENDIN_TAG:
				case self::L_CHR_ESCAP_TAG:
				case LexerInterface::L_TRASH:
				default:
					// ignore
					break;
			}
			
			$lexer->next();
		}
		if(null === $name)
		{
			$name = 'span';
		}
		$name = (string) \mb_strtolower($name);
		if(isset($this->_autoclosedTags[$name]))
		{
			return new HtmlSingleNode($name, $attr, '', true);
		}
		
		return new HtmlCollectionNode($name, $attr);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : HtmlAbstractNodeInterface
	{
		return parent::parse($data); // interface compatibility
	}
	
	/**
	 * Decodes the given html entities.
	 * 
	 * @param string $data
	 * @return string
	 */
	protected function decode(string $data) : string
	{
		return \html_entity_decode($data, \ENT_HTML5 | \ENT_QUOTES | \ENT_IGNORE, 'UTF-8');
	}
	
}
