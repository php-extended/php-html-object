<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

/**
 * HtmlTextNode class file.
 * 
 * This class represents a single raw node with no html decoration that encodes
 * for raw text.
 * 
 * @author Anastaszor
 */
class HtmlTextNode extends HtmlAbstractNode implements HtmlSingleNodeInterface
{
	
	/**
	 * The current value of the node.
	 * 
	 * @var string
	 */
	protected string $_text;
	
	/**
	 * Builds a new HtmlTextNode with the given value.
	 * 
	 * @param string $value
	 */
	public function __construct(string $value)
	{
		parent::__construct(HtmlAbstractNodeInterface::TYPE_TEXT);
		$this->_text = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::__toString()
	 */
	public function __toString() : string
	{
		return $this->escape($this->getValue());
	}
	
	/**
	 * Appends the given value to the current text.
	 * 
	 * @param string $value
	 */
	public function appendText(string $value) : void
	{
		$this->_text .= $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlSingleNodeInterface::getValue()
	 */
	public function getValue() : string
	{
		return $this->_text;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlSingleNodeInterface::isAutoclosed()
	 */
	public function isAutoclosed() : bool
	{
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::beVisitedBy()
	 */
	public function beVisitedBy(HtmlVisitorInterface $visitor)
	{
		return $visitor->visitSingle($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeContentLength()
	 */
	public function getTreeContentLength() : int
	{
		return (int) \mb_strlen($this->escape($this->getValue()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeHtmlLength()
	 */
	public function getTreeHtmlLength() : int
	{
		return $this->getTreeContentLength();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return 0 === (int) \mb_strlen(\trim($this->_text));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getText()
	 */
	public function getText() : string
	{
		return $this->_text;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof HtmlSingleNodeInterface
			&& $this->getValue() === $object->getValue()
			&& parent::equals($object);
	}
	
}
