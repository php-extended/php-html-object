<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

/**
 * HtmlDoctypeNode class file.
 * 
 * This class represents a doctype node with raw text.
 * 
 * @author Anastaszor
 */
class HtmlDoctypeNode extends HtmlAbstractNode implements HtmlSingleNodeInterface
{
	
	/**
	 * The current value of the node.
	 * 
	 * @var ?string
	 */
	protected ?string $_text = null;
	
	/**
	 * Builds a new HtmlTextNode with the given value.
	 * 
	 * @param string $value
	 */
	public function __construct(?string $value = null)
	{
		parent::__construct(HtmlAbstractNodeInterface::TYPE_DOCTYPE);
		$this->_text = $value;
		if(null === $this->_text || '' === $this->_text)
		{
			$this->_text = 'html'; // HTML 5 convention
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::__toString()
	 */
	public function __toString() : string
	{
		return '<!DOCTYPE '.$this->escape($this->getValue()).'>';
	}
	
	/**
	 * Appends the given value to the current text.
	 * 
	 * @param string $value
	 */
	public function appendText(string $value) : void
	{
		if(null === $this->_text)
		{
			$this->_text = '';
		}
		$this->_text .= $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlSingleNodeInterface::getValue()
	 */
	public function getValue() : string
	{
		return (string) $this->_text;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlSingleNodeInterface::isAutoclosed()
	 */
	public function isAutoclosed() : bool
	{
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::beVisitedBy()
	 */
	public function beVisitedBy(HtmlVisitorInterface $visitor)
	{
		return $visitor->visitSingle($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeHtmlLength()
	 */
	public function getTreeHtmlLength() : int
	{
		return 11 + (int) \mb_strlen($this->escape($this->getValue()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return 0 === (int) \mb_strlen(\trim($this->getText()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof HtmlSingleNodeInterface
			&& $this->getValue() === $object->getValue()
			&& parent::equals($object);
	}
	
}
