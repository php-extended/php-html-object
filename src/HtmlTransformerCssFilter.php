<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use PhpExtended\Css\CssAbstractSelectorInterface;

/**
 * HtmlTransformerCssFilter class file.
 * 
 * This class is a filter for html trees that removes nodes that do match
 * the given css filter given.
 * 
 * @author Anastaszor
 */
class HtmlTransformerCssFilter implements HtmlTransformerInterface
{

	/**
	 * The css selector used for filtering nodes.
	 * 
	 * @var CssAbstractSelectorInterface
	 */
	private $_cssFilter;

	/**
	 * Builds a new HtmlTransformerCssFilter based ont he given css selector.
	 * 
	 * @param CssAbstractSelectorInterface $cssSelector
	 */
	public function __construct(CssAbstractSelectorInterface $cssSelector)
	{
		$this->_cssFilter = $cssSelector;
	}

	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlTransformerInterface::transform()
	 */
	public function transform(HtmlCollectionNodeInterface $dom) : HtmlCollectionNodeInterface
	{
		$children = [];

		foreach($dom as $child)
		{
			if($this->_cssFilter->matches($child))
			{
				continue;
			}

			if($child instanceof HtmlCollectionNodeInterface)
			{
				$child = $this->transform($child);
			}

			$children[] = $child;
		}

		$attributes = [];
		
		foreach($dom->getAttributes() as $attribute)
		{
			$attributes[] = $attribute;
		}
		
		return new HtmlCollectionNode($dom->getName(), $attributes, $children);
	}

}
