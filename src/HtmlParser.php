<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;
use SplStack;

/**
 * HtmlParser class file.
 * 
 * This class is a simple implementation of the HtmlParserInterface based on a
 * char lexer.
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<HtmlCollectionNodeInterface>
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class HtmlParser extends AbstractParserLexer implements HtmlParserInterface
{
	
	public const L_CHR_ALPHANUM = 1; // alpha num
	public const L_CHR_WHITESPAC = 2; // sp
	public const L_CHR_START_TAG = 3; // <
	public const L_CHR_END_TAG = 4; // >
	public const L_CHR_SLASH_TAG = 5; // /
	public const L_CHR_ESCAP_TAG = 6; // \
	public const L_CHR_EQUAL_TAG = 7; // =
	public const L_CHR_DQUOT_TAG = 8; // "
	public const L_CHR_SQUOT_TAG = 9; // '
	public const L_CHR_SBRACKET = 10; // [
	public const L_CHR_EBRACKET = 11; // ]
	public const L_CHR_EXCLAM = 12; // !
	public const L_CHR_DASH = 13; // -
	public const L_CHR_COLON = 14; // :
	public const L_EXP_S1COMMENT = 17; // <!
	public const L_EXP_S2COMMENT = 18; // <!-
	public const L_TAG_COM_BUILD = 21; // <!--**
	public const L_TAG_COM_BUIL2 = 22; // <!--**-
	public const L_TAG_COM_BUIL3 = 23; // <!--**--
	public const L_TAG_COM_COMPL = 24; // <!--**-->
	public const L_TAG_END_BUILD = 25; // </
	public const L_TAG_END_BUIL2 = 26; // </**
	public const L_TAG_END_BUIL3 = 27; // </**sp
	public const L_TAG_END_COMPL = 28; // </**>
	public const L_TAG_DCT_BUILD = 29; // <!D**
	public const L_TAG_DCT_COMPL = 30; // <!D**>
	public const L_TAG_NOD_BUILD = 31; // <A
	public const L_TAG_NOD_BNAME = 32; // <A:
	public const L_TAG_NOD_BUIL2 = 33; // <Asp
	public const L_TAG_NOD_BUIL3 = 34; // <AspT
	public const L_TAG_NOD_BNAM2 = 35; // <AspT:
	public const L_TAG_NOD_B_EQL = 36; // <AspT=
	public const L_TAG_NOD_B_DQO = 37; // <AspT="**
	public const L_TAG_NOD_B_DQE = 38; // <AspT="**\    // end of attr goes to L_TAG_NOD_BUIL2
	public const L_TAG_NOD_B_SQO = 39; // <AspT='**
	public const L_TAG_NOD_B_SQE = 40; // <AspT='**\    // end of attr goes to L_TAG_NOD_BUIL2
	public const L_TAG_NOD_B_WQO = 41; // <AspT=**      // adding sp goes to
	public const L_TAG_NOD_ENDIN = 43; // <A/
	public const L_TAG_NOD_COMPL = 42; // <A>
	
	/**
	 * The html tag parser.
	 * 
	 * @var HtmlTagParser
	 */
	protected HtmlTagParser $_tagParser;
	
	/**
	 * Builds a new HtmlParser with the given lexer configuration.
	 * 
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function __construct()
	{
		parent::__construct(HtmlCollectionNodeInterface::class);
		$this->_tagParser = new HtmlTagParser();
		$this->_config->addMappings('<', self::L_CHR_START_TAG);
		$this->_config->addMappings('>', self::L_CHR_END_TAG);
		$this->_config->addMappings('/', self::L_CHR_SLASH_TAG);
		$this->_config->addMappings('\\', self::L_CHR_ESCAP_TAG);
		$this->_config->addMappings('=', self::L_CHR_EQUAL_TAG);
		$this->_config->addMappings('"', self::L_CHR_DQUOT_TAG);
		$this->_config->addMappings("'", self::L_CHR_SQUOT_TAG);
		$this->_config->addMappings('[', self::L_CHR_SBRACKET);
		$this->_config->addMappings(']', self::L_CHR_EBRACKET);
		$this->_config->addMappings('!', self::L_CHR_EXCLAM);
		$this->_config->addMappings('-', self::L_CHR_DASH);
		$this->_config->addMappings(':', self::L_CHR_COLON);
		$this->_config->addMappings(LexerInterface::CLASS_ALNUM, self::L_CHR_ALPHANUM);
		$this->_config->addMappings(LexerInterface::CLASS_SPACE, self::L_CHR_WHITESPAC);
		
		// add spaces to start tag
		$this->_config->addMerging(self::L_CHR_ALPHANUM, self::L_CHR_ALPHANUM, self::L_CHR_ALPHANUM);
		$this->_config->addMerging(self::L_CHR_WHITESPAC, self::L_CHR_WHITESPAC, self::L_CHR_WHITESPAC);
		$this->_config->addMerging(self::L_CHR_START_TAG, self::L_CHR_WHITESPAC, self::L_CHR_START_TAG);
		
		// detect comments
		$this->_config->addMerging(self::L_CHR_START_TAG, self::L_CHR_EXCLAM, self::L_EXP_S1COMMENT);
		$this->_config->addMerging(self::L_EXP_S1COMMENT, self::L_CHR_DASH, self::L_EXP_S2COMMENT);
		$this->_config->addMerging(self::L_EXP_S2COMMENT, self::L_CHR_DASH, self::L_TAG_COM_BUILD);
		
		// aggregate to comment (i.e. everything except dash)
		$this->_config->addMerging(self::L_TAG_COM_BUILD, self::L_CHR_DASH, self::L_TAG_COM_BUIL2); // <!--**-
		$this->_config->addMerging(self::L_TAG_COM_BUILD, LexerInterface::L_EVERYTHING_ELSE, self::L_TAG_COM_BUILD);
		
		$this->_config->addMerging(self::L_TAG_COM_BUIL2, self::L_CHR_DASH, self::L_TAG_COM_BUIL3); // <!--**--
		$this->_config->addMerging(self::L_TAG_COM_BUIL2, LexerInterface::L_EVERYTHING_ELSE, self::L_TAG_COM_BUILD);
		
		$this->_config->addMerging(self::L_TAG_COM_BUIL3, self::L_CHR_DASH, self::L_TAG_COM_BUIL3); // <!--**---
		$this->_config->addMerging(self::L_TAG_COM_BUIL3, self::L_CHR_END_TAG, self::L_TAG_COM_COMPL); // <!--**-->
		$this->_config->addMerging(self::L_TAG_COM_BUIL3, LexerInterface::L_EVERYTHING_ELSE, self::L_TAG_COM_BUILD);
		
		// TODO cdata sections
		
		// detect doctypes
		$this->_config->addMerging(self::L_EXP_S1COMMENT, self::L_CHR_ALPHANUM, self::L_TAG_DCT_BUILD); // <!D
		$this->_config->addMerging(self::L_TAG_DCT_BUILD, self::L_CHR_END_TAG, self::L_TAG_DCT_COMPL); // <!D>
		$this->_config->addMerging(self::L_TAG_DCT_BUILD, self::L_CHR_END_TAG, self::L_TAG_DCT_COMPL); // <!D**>
		$this->_config->addMerging(self::L_TAG_DCT_BUILD, LexerInterface::L_EVERYTHING_ELSE, self::L_TAG_DCT_BUILD);
		
		// end tags
		$this->_config->addMerging(self::L_CHR_START_TAG, self::L_CHR_SLASH_TAG, self::L_TAG_END_BUILD); // </
		$this->_config->addMerging(self::L_TAG_END_BUILD, self::L_CHR_WHITESPAC, self::L_TAG_END_BUILD);
		$this->_config->addMerging(self::L_TAG_END_BUILD, self::L_CHR_END_TAG, self::L_TAG_END_COMPL); // </>
		$this->_config->addMerging(self::L_CHR_START_TAG, self::L_CHR_ALPHANUM, self::L_TAG_END_BUIL2); // </**
		$this->_config->addMerging(self::L_TAG_END_BUILD, self::L_CHR_ALPHANUM, self::L_TAG_END_BUIL2);
		$this->_config->addMerging(self::L_TAG_END_BUIL2, self::L_CHR_ALPHANUM, self::L_TAG_END_BUIL2);
		$this->_config->addMerging(self::L_TAG_END_BUIL2, self::L_CHR_DASH, self::L_TAG_END_BUIL2);
		$this->_config->addMerging(self::L_TAG_END_BUIL2, self::L_CHR_END_TAG, self::L_TAG_END_COMPL); // </**>
		$this->_config->addMerging(self::L_TAG_END_BUIL2, self::L_CHR_WHITESPAC, self::L_TAG_END_BUIL3);
		$this->_config->addMerging(self::L_TAG_END_BUIL3, self::L_CHR_WHITESPAC, self::L_TAG_END_BUIL3); // </**sp
		$this->_config->addMerging(self::L_TAG_END_BUIL3, self::L_CHR_END_TAG, self::L_TAG_END_COMPL); // </**sp>
		
		// begin tags
		$this->_config->addMerging(self::L_CHR_START_TAG, self::L_CHR_ALPHANUM, self::L_TAG_NOD_BUILD); // <A
		$this->_config->addMerging(self::L_TAG_NOD_BUILD, self::L_CHR_ALPHANUM, self::L_TAG_NOD_BUILD); // <AA
		$this->_config->addMerging(self::L_TAG_NOD_BUILD, self::L_CHR_COLON, self::L_TAG_NOD_BNAME); // <A:
		$this->_config->addMerging(self::L_TAG_NOD_BNAME, self::L_CHR_ALPHANUM, self::L_TAG_NOD_BUILD); // <A:A
		$this->_config->addMerging(self::L_CHR_START_TAG, self::L_CHR_DASH, self::L_TAG_NOD_BUILD); // <A-
		$this->_config->addMerging(self::L_TAG_NOD_BUILD, self::L_CHR_END_TAG, self::L_TAG_NOD_COMPL); // <A>
		$this->_config->addMerging(self::L_TAG_NOD_BUILD, self::L_CHR_SLASH_TAG, self::L_TAG_NOD_ENDIN); // <A/
		$this->_config->addMerging(self::L_TAG_NOD_ENDIN, self::L_CHR_WHITESPAC, self::L_TAG_NOD_ENDIN); // <A/sp
		$this->_config->addMerging(self::L_TAG_NOD_ENDIN, self::L_CHR_END_TAG, self::L_TAG_NOD_COMPL); // <A/>
		
		$this->_config->addMerging(self::L_TAG_NOD_BUILD, self::L_CHR_WHITESPAC, self::L_TAG_NOD_BUIL2); // <Asp
		$this->_config->addMerging(self::L_TAG_NOD_BUIL2, self::L_CHR_WHITESPAC, self::L_TAG_NOD_BUIL2); // <Aspsp
		$this->_config->addMerging(self::L_TAG_NOD_BUIL2, self::L_CHR_END_TAG, self::L_TAG_NOD_COMPL); // <Asp>
		$this->_config->addMerging(self::L_TAG_NOD_BUIL2, self::L_CHR_SLASH_TAG, self::L_TAG_NOD_ENDIN); // <Asp/
		
		$this->_config->addMerging(self::L_TAG_NOD_BUIL2, self::L_CHR_ALPHANUM, self::L_TAG_NOD_BUIL3); // <AspT
		$this->_config->addMerging(self::L_TAG_NOD_BUIL3, self::L_CHR_ALPHANUM, self::L_TAG_NOD_BUIL3); // <AspT
		$this->_config->addMerging(self::L_TAG_NOD_BUIL3, self::L_CHR_COLON, self::L_TAG_NOD_BNAM2); // <AspT:
		$this->_config->addMerging(self::L_TAG_NOD_BNAM2, self::L_CHR_ALPHANUM, self::L_TAG_NOD_BUIL3); // <AspT:T
		$this->_config->addMerging(self::L_TAG_NOD_BUIL3, self::L_CHR_DASH, self::L_TAG_NOD_BUIL3); // <AspT-
		$this->_config->addMerging(self::L_TAG_NOD_BUIL3, self::L_CHR_WHITESPAC, self::L_TAG_NOD_BUIL2); // <AspTsp
		$this->_config->addMerging(self::L_TAG_NOD_BUIL3, self::L_CHR_END_TAG, self::L_TAG_NOD_COMPL); // <AspT>
		$this->_config->addMerging(self::L_TAG_NOD_BUIL3, self::L_CHR_SLASH_TAG, self::L_TAG_NOD_ENDIN); // <AspT/
		
		$this->_config->addMerging(self::L_TAG_NOD_BUIL3, self::L_CHR_EQUAL_TAG, self::L_TAG_NOD_B_EQL); // <AspT=
		$this->_config->addMerging(self::L_TAG_NOD_B_EQL, self::L_CHR_EQUAL_TAG, self::L_TAG_NOD_B_EQL); // <AspT==
		$this->_config->addMerging(self::L_TAG_NOD_B_EQL, self::L_CHR_WHITESPAC, self::L_TAG_NOD_BUIL2); // <AspT=sp
		$this->_config->addMerging(self::L_TAG_NOD_B_EQL, self::L_CHR_END_TAG, self::L_TAG_NOD_COMPL); // <AspT=>
		$this->_config->addMerging(self::L_TAG_NOD_B_EQL, self::L_CHR_SLASH_TAG, self::L_TAG_NOD_ENDIN); // <AspT=/
		
		$this->_config->addMerging(self::L_TAG_NOD_B_EQL, self::L_CHR_ALPHANUM, self::L_TAG_NOD_B_WQO); // <AspT=V
		$this->_config->addMerging(self::L_TAG_NOD_B_WQO, self::L_CHR_ALPHANUM, self::L_TAG_NOD_B_WQO); // <AspT=VV
		$this->_config->addMerging(self::L_TAG_NOD_B_WQO, self::L_CHR_DASH, self::L_TAG_NOD_B_WQO); // <AspT=V-
		$this->_config->addMerging(self::L_TAG_NOD_B_WQO, self::L_CHR_WHITESPAC, self::L_TAG_NOD_BUIL2); // <AspT=Vsp
		$this->_config->addMerging(self::L_TAG_NOD_B_WQO, self::L_CHR_END_TAG, self::L_TAG_NOD_COMPL); // <AspT=V>
		$this->_config->addMerging(self::L_TAG_NOD_B_WQO, self::L_CHR_SLASH_TAG, self::L_TAG_NOD_ENDIN); // <AspT=V/
		
		$this->_config->addMerging(self::L_TAG_NOD_B_EQL, self::L_CHR_DQUOT_TAG, self::L_TAG_NOD_B_DQO); // <AspT="
		$this->_config->addMerging(self::L_TAG_NOD_B_DQO, self::L_CHR_DQUOT_TAG, self::L_TAG_NOD_BUIL2); // <AspT="**"
		$this->_config->addMerging(self::L_TAG_NOD_B_DQO, self::L_CHR_ESCAP_TAG, self::L_TAG_NOD_B_DQE); // <AspT="**\
		$this->_config->addMerging(self::L_TAG_NOD_B_DQO, LexerInterface::L_EVERYTHING_ELSE, self::L_TAG_NOD_B_DQO); // <AspT="**
		$this->_config->addMerging(self::L_TAG_NOD_B_DQE, LexerInterface::L_EVERYTHING_ELSE, self::L_TAG_NOD_B_DQO); // <AspT="**\\
		
		$this->_config->addMerging(self::L_TAG_NOD_B_EQL, self::L_CHR_SQUOT_TAG, self::L_TAG_NOD_B_SQO); // <AspT='
		$this->_config->addMerging(self::L_TAG_NOD_B_SQO, self::L_CHR_ESCAP_TAG, self::L_TAG_NOD_B_SQE); // <AspT='**\
		$this->_config->addMerging(self::L_TAG_NOD_B_SQO, self::L_CHR_SQUOT_TAG, self::L_TAG_NOD_BUIL2); // <AspT='**'
		$this->_config->addMerging(self::L_TAG_NOD_B_SQO, LexerInterface::L_EVERYTHING_ELSE, self::L_TAG_NOD_B_SQO); // <AspT='**
		$this->_config->addMerging(self::L_TAG_NOD_B_SQE, LexerInterface::L_EVERYTHING_ELSE, self::L_TAG_NOD_B_SQO); // <AspT='**\\
	}
	
	/**
	 * Parses the data that comes from the given lexer.
	 * 
	 * @param LexerInterface $lexer
	 * @return HtmlCollectionNodeInterface
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function parseLexer(LexerInterface $lexer) : HtmlCollectionNodeInterface
	{
		// token stack of only collection nodes
		/** @var SplStack<HtmlCollectionNode> $tokenStack */
		$tokenStack = new SplStack(); // use only push, pop and is empty
		$rootNode = new HtmlCollectionNode(HtmlCollectionNode::TYPE_DOCUMENT);
		
		// build the token stack based on the parsing of the data
		$lexer->rewind();
		
		while($lexer->valid())
		{
			if($tokenStack->isEmpty())
			{
				$tokenStack->push($rootNode);
			}
			
			/** @var \PhpExtended\Lexer\Lexeme $token */
			$token = $lexer->current();
			
			switch($token->getCode())
			{
				case self::L_TAG_COM_BUILD:
				case self::L_TAG_COM_BUIL2:
				case self::L_TAG_COM_BUIL3:
				case self::L_TAG_COM_COMPL:
					$value = \trim($token->getData());
					$value = \ltrim($value, '<');
					$value = \rtrim($value, '>');
					$value = \trim($value);
					$value = \ltrim($value, '!');
					$value = \trim($value);
					$value = \trim($value, '-');
					$newNode = new HtmlCommentNode($this->decode($value));
					$tokenStack->top()->addChild($newNode);
					break;
					
				case self::L_TAG_DCT_BUILD:
				case self::L_TAG_DCT_COMPL:
					$value = \trim($token->getData());
					$value = \ltrim($value, '<');
					$value = \rtrim($value, '>');
					$value = \trim($value);
					$value = \ltrim($value, '!');
					$value = \trim($value);
					$newNode = new HtmlDoctypeNode($this->decode($value));
					$tokenStack->top()->addChild($newNode);
					break;
					
				case self::L_TAG_END_BUILD:
				case self::L_TAG_END_BUIL2:
				case self::L_TAG_END_BUIL3:
				case self::L_TAG_END_COMPL:
					$value = \trim($token->getData());
					$value = \ltrim($value, '<');
					$value = \rtrim($value, '>');
					$value = \trim($value);
					$value = \ltrim($value, '/');
					$value = \trim($value);
					$value = $this->decode($value);
					if(empty($value))
					{
						break;
					}
					if($tokenStack->top()->getName() === $value)
					{
						$tokenStack->pop();
						break;
					}
					// search for the whole stack for the next node to free
					$rstack = [];
					
					foreach($tokenStack as $token)
					{
						$rstack[] = $token;
					}
					
					$rstack = \array_reverse($rstack);
					$popnb = 0;
					
					foreach($rstack as $k => $node)
					{
						if($node->getName() === $value)
						{
							$popnb = $k + 1;
							break;
						}
					}
					
					while(0 < $popnb--)
					{
					$tokenStack->pop();
					}
					break;
					
				case self::L_TAG_NOD_BUILD:
				case self::L_TAG_NOD_BUIL2:
				case self::L_TAG_NOD_BUIL3:
				case self::L_TAG_NOD_B_EQL:
				case self::L_TAG_NOD_B_DQO:
				case self::L_TAG_NOD_B_DQE:
				case self::L_TAG_NOD_B_SQO:
				case self::L_TAG_NOD_B_SQE:
				case self::L_TAG_NOD_B_WQO:
				case self::L_TAG_NOD_ENDIN:
				case self::L_TAG_NOD_COMPL:
				case self::L_TAG_NOD_BNAME:
				case self::L_TAG_NOD_BNAM2:
					$node = $this->_tagParser->parse($token->getData());
					$tokenStack->top()->addChild($node);
					if($node instanceof HtmlCollectionNode)
					{
						$tokenStack->push($node);
					}
					break;
				
				case self::L_CHR_ALPHANUM:
				case self::L_CHR_DASH:
				case self::L_CHR_COLON:
				case self::L_CHR_DQUOT_TAG:
				case self::L_CHR_EBRACKET:
				case self::L_CHR_END_TAG:
				case self::L_CHR_EQUAL_TAG:
				case self::L_CHR_ESCAP_TAG:
				case self::L_CHR_EXCLAM:
				case self::L_CHR_SBRACKET:
				case self::L_CHR_SLASH_TAG:
				case self::L_CHR_SQUOT_TAG:
				case self::L_CHR_START_TAG:
				case self::L_CHR_WHITESPAC:
				case self::L_EXP_S1COMMENT:
				case self::L_EXP_S2COMMENT:
				default:
					$newNode = new HtmlTextNode($this->decode($token->getData()));
					$tokenStack->top()->addChild($newNode);
					break;
			}
			
			$lexer->next();
		}
		
		return $rootNode;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlParserInterface::parseString()
	 */
	public function parse(?string $data) : HtmlCollectionNodeInterface
	{
		return parent::parse($data); // interface compatibility
	}
	
	/**
	 * Decodes the given html entities.
	 * 
	 * @param string $data
	 * @return string
	 */
	protected function decode(string $data) : string
	{
		return \html_entity_decode($data, \ENT_HTML5 | \ENT_QUOTES | \ENT_IGNORE, 'UTF-8');
	}
	
}
