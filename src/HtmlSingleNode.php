<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

/**
 * HtmlSingleNode class file.
 * 
 * This class is a simple implementation of the HtmlSingleNodeInterface.
 * 
 * @author Anastaszor
 */
class HtmlSingleNode extends HtmlAbstractNode implements HtmlSingleNodeInterface
{
	
	/**
	 * The inner value of the node.
	 * 
	 * @var string
	 */
	protected string $_value;
	
	/**
	 * Whether this node is autoclosed.
	 * 
	 * @var boolean
	 */
	protected bool $_autoclosed = false;
	
	/**
	 * Builds a new HtmlSingleNode with the given.
	 * 
	 * @param string $name
	 * @param array<integer, HtmlAttributeInterface>|array<string, string> $attributes
	 * @param string $value
	 * @param boolean $autoclosed
	 */
	public function __construct(string $name, array $attributes = [], string $value = '', bool $autoclosed = false)
	{
		parent::__construct($name, $attributes);
		$this->_value = $value;
		$this->_autoclosed = $autoclosed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::__toString()
	 */
	public function __toString() : string
	{
		$str = '<'.$this->escape($this->getName()).($this->isEmpty() ? '' : ' '.$this->getAttributes()->__toString());
		if($this->_autoclosed)
		{
			return $str.' />';
		}
		
		return $str.'>'.$this->escape($this->getValue()).'</'.$this->escape($this->getName()).'>';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlSingleNodeInterface::getValue()
	 */
	public function getValue() : string
	{
		return $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlSingleNodeInterface::isAutoclosed()
	 */
	public function isAutoclosed() : bool
	{
		return $this->_autoclosed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::beVisitedBy()
	 */
	public function beVisitedBy(HtmlVisitorInterface $visitor)
	{
		return $visitor->visitSingle($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeContentLength()
	 */
	public function getTreeContentLength() : int
	{
		return (int) \mb_strlen($this->escape($this->getValue()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeHtmlLength()
	 */
	public function getTreeHtmlLength() : int
	{
		return 5 + 2 * (int) \mb_strlen($this->escape($this->getName())) 
			+ ($this->isEmpty() ? 0 : 1 + (int) \mb_strlen($this->getAttributes()->__toString()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return empty($this->_value) && parent::isEmpty();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof HtmlSingleNodeInterface
			&& $this->getValue() === $object->getValue()
			&& parent::equals($object);
	}
	
}
