<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

/**
 * HtmlAttributeList class file.
 * 
 * This class is a simple implementation of the HtmlAttributeList.
 * 
 * @author Anastaszor
 */
class HtmlAttributeList implements HtmlAttributeListInterface
{
	
	/**
	 * The attribute items.
	 * 
	 * @var array<string, HtmlAttributeInterface>
	 */
	protected array $_attributes = [];
	
	/**
	 * Builds a new HtmlAttributeList with the given list of attributes.
	 * 
	 * @param array<integer, HtmlAttributeInterface> $list
	 */
	public function __construct(array $list = [])
	{
		foreach($list as $attr)
		{
			$this->setAttribute($attr);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeListInterface::__toString()
	 */
	public function __toString() : string
	{
		$str = [];
		
		foreach($this as $attribute)
		{
			$str[] = $attribute->__toString();
		}
		
		return \implode(' ', $str);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeListInterface::hasAttribute()
	 */
	public function hasAttribute(string $name) : bool
	{
		$name = (string) \mb_strtolower($name);
		
		return isset($this->_attributes[$name]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeListInterface::getAttribute()
	 */
	public function getAttribute(string $name) : ?HtmlAttributeInterface
	{
		$name = (string) \mb_strtolower($name);
		if(isset($this->_attributes[$name]))
		{
			return $this->_attributes[$name];
		}
		
		return null;
	}
	
	/**
	 * Sets the attribute for the given attribute name.
	 * 
	 * @param HtmlAttributeInterface $attr
	 */
	public function setAttribute(HtmlAttributeInterface $attr) : void
	{
		$this->_attributes[$attr->getName()] = $attr;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_attributes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeListInterface::current()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function current() : HtmlAttributeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return \current($this->_attributes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		\next($this->_attributes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : string
	{
		return (string) \key($this->_attributes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return false !== \current($this->_attributes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		\reset($this->_attributes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeListInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return 0 === \count($this->_attributes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeListInterface::equals()
	 */
	public function equals($object) : bool
	{
		if(!$object instanceof HtmlAttributeListInterface)
		{
			return false;
		}
		
		// perform double inclusion
		foreach($this as $attribute)
		{
			if(!$attribute->equals($object->getAttribute($attribute->getName())))
			{
				return false;
			}
		}
		
		foreach($object as $otherAttr)
		{
			if(!$otherAttr->equals($this->getAttribute($otherAttr->getName())))
			{
				return false;
			}
		}
		
		return true;
	}
	
}
