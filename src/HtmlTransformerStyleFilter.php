<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

/**
 * HtmlTransformerStyleFilter class file.
 * 
 * This class is a filter for html trees that removes style nodes and style
 * attributes.
 * 
 * @author Anastaszor
 */
class HtmlTransformerStyleFilter implements HtmlTransformerInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlTransformerInterface::transform()
	 */
	public function transform(HtmlCollectionNodeInterface $dom) : HtmlCollectionNodeInterface
	{
		$children = [];
		
		foreach($dom as $child)
		{
			if($child->getName() === 'link')
			{
				$attribute = $child->getAttribute('rel');
				if(null !== $attribute && $attribute->valueContains('stylesheet'))
				{
					continue;
				}
			}
			
			if($child instanceof HtmlCollectionNodeInterface)
			{
				$child = $this->transform($child);
			}
			
			$children[] = $child;
		}
		
		$attributes = [];
		
		foreach($dom->getAttributes() as $attribute)
		{
			if('style' === $attribute->getName())
			{
				continue;
			}
			
			$attributes[] = $attribute;
		}
		
		return new HtmlCollectionNode($dom->getName(), $attributes, $children);
	}
	
}
