<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

/**
 * HtmlTransformerScriptFilter class file.
 * 
 * This class is a filter for html trees that removes script nodes and script
 * attributes.
 * 
 * @author Anastaszor
 */
class HtmlTransformerScriptFilter implements HtmlTransformerInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlTransformerInterface::transform()
	 */
	public function transform(HtmlCollectionNodeInterface $dom) : HtmlCollectionNodeInterface
	{
		$children = [];
		
		foreach($dom as $child)
		{
			if('script' === $child->getName())
			{
				continue;
			}
			
			if($child instanceof HtmlCollectionNodeInterface)
			{
				$child = $this->transform($child);
			}
			
			$children[] = $child;
		}
		
		$attributes = [];
		
		foreach($dom->getAttributes() as $attribute)
		{
			if(0 === \mb_strpos($attribute->getName(), 'on')) // at start
			{
				continue;
			}
			
			$attributes[] = $attribute;
		}
		
		return new HtmlCollectionNode($dom->getName(), $attributes, $children);
	}
	
}
