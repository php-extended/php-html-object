<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use Iterator;
use PhpExtended\Css\CssSelectorParser;
use PhpExtended\Css\CssSelectorParserInterface;

/**
 * HtmlAbstractNode class file.
 * 
 * This class is a simple implementation of the HtmlAbstractNodeInterface.
 * 
 * @author Anastaszor
 */
abstract class HtmlAbstractNode implements HtmlAbstractNodeInterface
{
	
	/**
	 * The singleton css parser.
	 * 
	 * @var ?CssSelectorParserInterface
	 */
	protected static ?CssSelectorParserInterface $_cssParser = null;
	
	/**
	 * Gets the singleton css parser.
	 * 
	 * @return CssSelectorParserInterface
	 */
	protected static function getCssParser() : CssSelectorParserInterface
	{
		if(null === self::$_cssParser)
		{
			self::$_cssParser = new CssSelectorParser();
		}
		
		return self::$_cssParser;
	}
	
	/**
	 * The name of the node.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The attribute values (name => value).
	 * 
	 * @var array<string, HtmlAttributeInterface>
	 */
	protected array $_attributes = [];
	
	/**
	 * Builds a new HtmlAbstractNode with the given name and attributes values.
	 * 
	 * @param string $name
	 * @param array<integer, HtmlAttributeInterface>|array<string, string> $attributes
	 */
	public function __construct(string $name, array $attributes = [])
	{
		$this->_name = \trim((string) \mb_strtolower($name));
		
		foreach($attributes as $name => $value)
		{
			if($value instanceof HtmlAttributeInterface)
			{
				$this->_attributes[$value->getName()] = $value;
				continue;
			}
			
			$this->setAttribute((string) $name, (string) $value);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::__toString()
	 */
	public function __toString() : string
	{
		return '<'.$this->escape($this->getName()).($this->isEmpty() ? '' : ' '.$this->getAttributes()->__toString()).'>'
			  .'</'.$this->escape($this->getName()).'>';
	}
	
	/**
	 * Helper function to encode html text.
	 * 
	 * @param string $data
	 * @return string
	 */
	public function escape(string $data) : string
	{
		// every character except those 4 are not needed for this library
		return \strtr($data, [
			'&' => '&amp;',
			'<' => '&lt;',
			'>' => '&gt;',
			'"' => '&quot;',
			'\'' => '&#39;',
			'`' => '&grave;',
		]);
	}
	
	/**
	 * Sets the name of this node with the given name value.
	 * 
	 * @param string $name
	 */
	public function setName(string $name) : void
	{
		$this->_name = $name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::hasAttribute()
	 */
	public function hasAttribute(string $name) : bool
	{
		$name = \trim((string) \mb_strtolower($name));
		
		return isset($this->_attributes[$name]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getAttribute()
	 */
	public function getAttribute(string $name) : ?HtmlAttributeInterface
	{
		$name = \trim((string) \mb_strtolower($name));
		if(isset($this->_attributes[$name]))
		{
			return $this->_attributes[$name];
		}
		
		return null;
	}
	
	/**
	 * Sets the attribute with the given name to the given value.
	 * 
	 * @param string $name
	 * @param string $value
	 */
	public function setAttribute(string $name, string $value = '') : void
	{
		$name = \trim((string) \mb_strtolower($name));
		$this->_attributes[$name] = new HtmlAttribute($name, $value);
	}
	
	/**
	 * Appends the given value to the given attribute with a space as
	 * separator.
	 * 
	 * @param string $name
	 * @param string $value
	 */
	public function appendAttribute(string $name, string $value = '') : void
	{
		$name = \trim((string) \mb_strtolower($name));
		if(isset($this->_attributes[$name]))
		{
			$this->_attributes[$name] = new HtmlAttribute($name, $this->_attributes[$name]->getValue().' '.$value);
			
			return;
		}
		
		$this->_attributes[$name] = new HtmlAttribute($name, $value);
	}
	
	/**
	 * Unsets the attribute with the given name.
	 * 
	 * @param string $name
	 */
	public function unsetAttribute(string $name) : void
	{
		$name = \trim((string) \mb_strtolower($name));
		unset($this->_attributes[$name]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getAttributes()
	 */
	public function getAttributes() : HtmlAttributeListInterface
	{
		return new HtmlAttributeList(\array_values($this->_attributes));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		if('input' === $this->_name)
		{
			return false;
		}
		
		if('textarea' === $this->_name)
		{
			return false;
		}
		
		// TODO handle svg children nodes
		
		foreach($this->_attributes as $attrname => $attrVal)
		{
			if((
				\in_array((string) $attrname, [
					'content', // meta
					'value',   // data, meter, option, param
					'shape',   // area
					'title',   // abbr
					'data',    // object
				], true) || \mb_strpos((string) $attrname, 'data-') !== false
			) && 0 < (int) \mb_strlen(\trim($attrVal->getValue())))
			{
				return false;
			}
			
			if(\in_array((string) $attrname, [
				'src',     // embed, iframe, img, source, track
				'href',    // a:not([href=#]), link:not([href=/])
			], true) && 1 < (int) \mb_strlen(\trim($attrVal->getValue())))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getText()
	 */
	public function getText() : string
	{
		return '';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof HtmlAbstractNodeInterface
			&& $this->getAttributes()->equals($object->getAttributes());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::dump()
	 */
	public function dump(string $indent = self::INDENT_TABS, int $depths = 0) : string
	{
		return \str_repeat($indent, $depths).$this->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeNodeQuantity()
	 */
	public function getTreeNodeQuantity() : int
	{
		return 1;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeDepths()
	 */
	public function getTreeDepths() : int
	{
		return 1;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeContentLength()
	 */
	public function getTreeContentLength() : int
	{
		return 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::matchesCss()
	 */
	public function matchesCss(string $cssSelector) : bool
	{
		$selector = $this->getCssParser()->parse($cssSelector);
		
		return $selector->matches($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::findNodeCss()
	 */
	public function findNodeCss(string $cssSelector, int $position = 0) : ?HtmlAbstractNodeInterface
	{
		$selector = $this->getCssParser()->parse($cssSelector);
		
		return $selector->findNode($this, $position);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::findAllNodesCss()
	 */
	public function findAllNodesCss(string $cssSelector) : Iterator
	{
		$selector = $this->getCssParser()->parse($cssSelector);
		
		return $selector->findAllNodes($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::filterCss()
	 */
	public function filterCss(string $cssSelector) : ?HtmlAbstractNodeInterface
	{
		$selector = $this->getCssParser()->parse($cssSelector);
		
		if($selector->matches($this))
		{
			return null;
		}

		return $this;
	}

}
