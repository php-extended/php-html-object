<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

/**
 * HtmlAttribute class file.
 * 
 * This class is a simple implementation of the HtmlAttributeInterface.
 * 
 * @author Anastaszor
 */
class HtmlAttribute implements HtmlAttributeInterface
{
	
	/**
	 * The name of the attribute.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The values of the attribute.
	 * 
	 * @var array<integer, string>
	 */
	protected array $_values = [];
	
	/**
	 * Builds a new HtmlAttribute with the given name and value.
	 * 
	 * @param string $name
	 * @param string $value
	 */
	public function __construct(string $name, string $value = '')
	{
		$this->_name = \trim((string) \mb_strtolower($name));
		/** @phpstan-ignore-next-line array_filter+strlen */
		$this->_values = \array_filter(\explode(' ', $value), 'strlen');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeInterface::__toString()
	 */
	public function __toString() : string
	{
		if(empty($this->_values))
		{
			return $this->escape($this->_name);
		}
		
		return $this->escape($this->_name).'="'.$this->escape($this->getValue()).'"';
	}
	
	/**
	 * Sets the name of the attribute.
	 * 
	 * @param string $name
	 */
	public function setName(string $name) : void
	{
		$this->_name = \trim((string) \mb_strtolower($name));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the value of this attribute.
	 * 
	 * @param string $value
	 */
	public function setValue(string $value) : void
	{
		$this->_values = \explode(' ', $value);
	}
	
	/**
	 * Adds the given value to this attribute.
	 * 
	 * @param string $value
	 */
	public function addValue(string $value) : void
	{
		if(!empty($value))
		{
			$this->_values[] = $value;
		}
	}
	
	/**
	 * Removes the values of this attribute.
	 */
	public function clearValues() : void
	{
		$this->_values = [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeInterface::getValue()
	 */
	public function getValue() : string
	{
		return \implode(' ', $this->_values);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeInterface::valueBeginsWith()
	 */
	public function valueBeginsWith(string $value) : bool
	{
		return 0 === \mb_strpos($this->getValue(), $value);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeInterface::valueContains()
	 */
	public function valueContains(string $value) : bool
	{
		return false !== \mb_strpos($this->getValue(), $value);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeInterface::valueEndsWith()
	 */
	public function valueEndsWith(string $value) : bool
	{
		return (int) \mb_strlen($this->getValue()) - (int) \mb_strlen($value) === \mb_strpos($this->getValue(), $value);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeInterface::valueEquals()
	 */
	public function valueEquals(string $value) : bool
	{
		return $this->getValue() === $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeInterface::valueWordBeginsWith()
	 */
	public function valueWordBeginsWith(string $value) : bool
	{
		foreach($this->_values as $part)
		{
			if($part === $value || false !== \mb_strpos($part, $value.'-'))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeInterface::valueHasWord()
	 */
	public function valueHasWord(string $value) : bool
	{
		return \in_array($value, $this->_values, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAttributeInterface::equals()
	 */
	public function equals($object) : bool
	{
		if(!$object instanceof HtmlAttributeInterface)
		{
			return false;
		}
		
		if($this->getName() !== $object->getName())
		{
			return false;
		}
		
		/** @phpstan-ignore-next-line array_filter+strlen */
		$otherAttr = \array_filter(\explode(' ', $object->getValue()), 'strlen');
		
		// perform double inclusion
		foreach($this->_values as $val)
		{
			if(!\in_array($val, $otherAttr, true))
			{
				return false;
			}
		}
		
		foreach($otherAttr as $otherVal)
		{
			if(!\in_array($otherVal, $this->_values, true))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Helper function to encode html text.
	 *
	 * @param string $data
	 * @return string
	 */
	public function escape(string $data) : string
	{
		// every character except those 4 are not needed for this library
		return \strtr($data, [
			'&' => '&amp;',
			'<' => '&lt;',
			'>' => '&gt;',
			'"' => '&quot;',
			'\'' => '&#39;',
			'`' => '&grave;',
		]);
	}
	
}
