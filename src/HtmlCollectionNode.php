<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use ArrayIterator;
use Iterator;

/**
 * HtmlCollectionNode class file.
 * 
 * This class is a simple implementation of the HtmlCollectionNodeInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class HtmlCollectionNode extends HtmlAbstractNode implements HtmlCollectionNodeInterface
{
	
	/**
	 * The children of this node.
	 * 
	 * @var array<integer, HtmlAbstractNodeInterface>
	 */
	protected array $_children = [];
	
	/**
	 * Builds a new HtmlCollectionNode with the given name, attributes and
	 * children nodes.
	 * 
	 * @param string $name
	 * @param array<integer, HtmlAttributeInterface>|array<string, string> $attributes
	 * @param array<integer, HtmlAbstractNodeInterface> $children
	 */
	public function __construct(string $name, array $attributes = [], array $children = [])
	{
		parent::__construct($name, $attributes);
		
		foreach($children as $child)
		{
			$this->addChild($child);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::__toString()
	 */
	public function __toString() : string
	{
		$parts = [];
		
		foreach($this->_children as $child)
		{
			$parts[] = $child->__toString();
		}
		
		if($this->getName() === static::TYPE_DOCUMENT)
		{
			return \implode('', $parts);
		}
		
		$before = '<'.$this->escape($this->getName()).($this->getAttributes()->isEmpty() ? '' : ' '.$this->getAttributes()->__toString()).'>';
		$after = '</'.$this->escape($this->getName()).'>';
		
		return $before.\implode('', $parts).$after;
	}
	
	/**
	 * Adds a child node to this node.
	 * 
	 * @param HtmlAbstractNodeInterface $child
	 */
	public function addChild(HtmlAbstractNodeInterface $child) : void
	{
		if(0 < \count($this->_children))
		{
			$last = \end($this->_children);
			// merge text nodes of children
			if($child instanceof HtmlTextNode && $last instanceof HtmlTextNode)
			{
				\array_pop($this->_children);
				$child = new HtmlTextNode($last->getValue().$child->getValue());
			}
		}
		$this->_children[] = $child;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlCollectionNodeInterface::countChildren()
	 */
	public function countChildren(?string $type = null) : int
	{
		if(null === $type)
		{
			return $this->count();
		}
		
		$type = \trim((string) \mb_strtolower($type));
		
		$count = 0;
		
		foreach($this->_children as $child)
		{
			if($child->getName() === $type)
			{
				$count++;
			}
		}
		
		return $count;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlCollectionNodeInterface::hasChildren()
	 */
	public function hasChildren(?string $type = null) : bool
	{
		if(null === $type)
		{
			return !empty($this->_children);
		}
		
		$type = \trim((string) \mb_strtolower($type));
		
		foreach($this->_children as $child)
		{
			if($child->getName() === $type)
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlCollectionNodeInterface::getFirstChild()
	 */
	public function getFirstChild(?string $type = null, int $position = 0) : ?HtmlAbstractNodeInterface
	{
		if(0 > $position)
		{
			return $this->getLastChild($type, -$position - 1);
		}
		
		if(null !== $type)
		{
			$type = \trim((string) \mb_strtolower($type));
		}
		
		if(\count($this->_children) < $position)
		{
			return null;
		}
		
		foreach($this->_children as $child)
		{
			if(null === $type || $child->getName() === $type)
			{
				if(0 === $position)
				{
					return $child;
				}
				$position--;
			}
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlCollectionNodeInterface::getLastChild()
	 */
	public function getLastChild(?string $type = null, int $position = 0) : ?HtmlAbstractNodeInterface
	{
		if(0 > $position)
		{
			return $this->getFirstChild($type, -(1 + $position));
		}
		
		if(null !== $type)
		{
			$type = \trim((string) \mb_strtolower($type));
		}
		
		if(\count($this->_children) < $position)
		{
			return null;
		}
		
		foreach(\array_reverse($this->_children) as $child)
		{
			if(null === $type || $child->getName() === $type)
			{
				if(0 === $position)
				{
					return $child;
				}
				$position--;
			}
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlCollectionNodeInterface::getChildren()
	 */
	public function getChildren(?string $type = null) : HtmlCollectionNodeInterface
	{
		if(null === $type)
		{
			return new HtmlCollectionNode($this->getName(), \array_values($this->_attributes), $this->_children);
		}
		
		$list = [];
		
		foreach($this->_children as $child)
		{
			if($child->getName() === $type)
			{
				$list[] = $child;
			}
		}
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress ImplicitToStringCast */
		return new HtmlCollectionNode($this->getName(), $list, $this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlCollectionNodeInterface::current()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function current() : HtmlAbstractNodeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return \current($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		\next($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : string
	{
		return (string) \key($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return false !== \current($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		\reset($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::beVisitedBy()
	 */
	public function beVisitedBy(HtmlVisitorInterface $visitor)
	{
		return $visitor->visitCollection($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeNodeQuantity()
	 */
	public function getTreeNodeQuantity() : int
	{
		$qty = 1;
		
		foreach($this as $child)
		{
			$qty += $child->getTreeNodeQuantity();
		}
		
		return $qty;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeDepths()
	 */
	public function getTreeDepths() : int
	{
		$depths = 0;
		
		foreach($this as $child)
		{
			$depths = \max($depths, $child->getTreeDepths());
		}
		
		return 1 + $depths;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeContentLength()
	 */
	public function getTreeContentLength() : int
	{
		$length = 0;
		
		foreach($this as $child)
		{
			$length += $child->getTreeContentLength();
		}
		
		return $length;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getTreeHtmlLength()
	 */
	public function getTreeHtmlLength() : int
	{
		$tree = 5 + 2 * (int) \mb_strlen($this->escape($this->getName())) 
			+ ($this->isEmpty() ? 0 : 1 + (int) \mb_strlen($this->getAttributes()->__toString()));
		
		foreach($this as $child)
		{
			$tree += $child->getTreeHtmlLength();
		}
		
		return $tree;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::isEmpty()
	 */
	public function isEmpty() : bool
	{
		foreach($this->_children as $child)
		{
			if(!$child->isEmpty())
			{
				return false;
			}
		}
		
		return parent::isEmpty();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::getText()
	 */
	public function getText() : string
	{
		$str = [];
		
		foreach($this->_children as $child)
		{
			$str[] = $child->getText();
		}
		
		return \implode('', $str);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNode::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof HtmlCollectionNodeInterface
			&& $object->count() === $this->count()
			&& parent::equals($object);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::dump()
	 */
	public function dump(string $indent = self::INDENT_TABS, int $depths = 0) : string
	{
		$parts = [];
		
		foreach($this->_children as $child)
		{
			$parts[] = $child->dump($indent, $depths + 1);
		}
		
		$indents = \str_repeat($indent, $depths);
		if($this->getName() === static::TYPE_DOCUMENT)
		{
			return $indents.\implode("\n", $parts);
		}
		
		$before = $indents.'<'.$this->escape($this->getName()).($this->getAttributes()->isEmpty() ? '' : ' '.$this->getAttributes()->__toString()).'>';
		$after = $indents.'</'.$this->escape($this->getName()).'>';
		
		return $before."\n".\implode("\n", $parts)."\n".$after;
	}
	
	// ---- ---- ---- ---- Recursive methods ---- ---- ---- ---- \\
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlCollectionNodeInterface::getElementById()
	 */
	public function getElementById(string $ident) : ?HtmlAbstractNodeInterface
	{
		$attribute = $this->getAttribute('id');
		if(null !== $attribute && $attribute->valueEquals($ident))
		{
			return $this;
		}
		
		foreach($this->_children as $child)
		{
			if($child instanceof HtmlCollectionNodeInterface)
			{
				$element = $child->getElementById($ident);
				if(null !== $element)
				{
					return $element;
				}
			}
			$attribute = $child->getAttribute('id');
			if(null !== $attribute && $attribute->getName() === $ident)
			{
				return $child;
			}
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlCollectionNodeInterface::getElementsByTagName()
	 */
	public function getElementsByTagName(string $name) : Iterator
	{
		$name = \trim((string) \mb_strtolower($name));
		$elements = [];
		
		if($this->getName() === $name)
		{
			$elements[] = $this;
		}
		
		foreach($this->_children as $child)
		{
			if($child instanceof HtmlCollectionNodeInterface)
			{
				foreach($child->getElementsByTagName($name) as $element)
				{
					$elements[] = $element;
				}
			}
			if($child->getName() === $name)
			{
				$elements[] = $child;
			}
		}
		
		return new ArrayIterator($elements);
	}
	
	/**
	 * Removes all the nodes that have no attributes and no contents,
	 * recursively, on all nodes that are present in the subtree represented
	 * by this node.
	 *
	 * @return HtmlCollectionNodeInterface
	 */
	public function removeEmpty() : HtmlCollectionNodeInterface
	{
		return $this->transformWith(new HtmlTransformerEmptyFilter());
	}
	
	/**
	 * Removes all the nodes that are of type CData on all nodes that are
	 * present in the subtree represented by this node.
	 *
	 * @return HtmlCollectionNodeInterface
	 */
	public function removeCData() : HtmlCollectionNodeInterface
	{
		return $this->transformWith(new HtmlTransformerCDataFilter());
	}
	
	/**
	 * Removes all the comments on attributes and all comment nodes that are
	 * present in the subtree represented by this node.
	 *
	 * @return HtmlCollectionNodeInterface
	 */
	public function removeComments() : HtmlCollectionNodeInterface
	{
		return $this->transformWith(new HtmlTransformerCommentFilter());
	}
	
	/**
	 * Removes all the scripts on attributes and all script nodes that are
	 * present in the subtree represented by this node.
	 *
	 * @return HtmlCollectionNodeInterface
	 */
	public function removeScripts() : HtmlCollectionNodeInterface
	{
		return $this->transformWith(new HtmlTransformerScriptFilter());
	}
	
	/**
	 * Removes all the styles on attributes and all style nodes that are
	 * present in the subtree represented by this node.
	 *
	 * @return HtmlCollectionNodeInterface
	 */
	public function removeStyles() : HtmlCollectionNodeInterface
	{
		return $this->transformWith(new HtmlTransformerStyleFilter());
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Html\HtmlAbstractNodeInterface::filterCss()
	 */
	public function filterCss(string $cssSelector) : ?HtmlAbstractNodeInterface
	{
		$selector = $this->getCssParser()->parse($cssSelector);

		$transformer = new HtmlTransformerCssFilter($selector);

		return $this->transformWith($transformer);
	}
	
	/**
	 * Transforms the html tree structure represented by this node to another
	 * structure according to the rules of the given transformer.
	 *
	 * @param HtmlTransformerInterface $transformer
	 * @return HtmlCollectionNodeInterface
	 */
	public function transformWith(HtmlTransformerInterface $transformer) : HtmlCollectionNodeInterface
	{
		return $transformer->transform($this);
	}
	
}
