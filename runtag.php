<?php declare(strict_types=1);

/**
 * Test script for html tag parser.
 * 
 * This file will give the data in object form.
 * 
 * Usage:
 * php testtag.php "<string_tag>"
 * 
 * @author Anastaszor
 */

use PhpExtended\Html\HtmlTagParser;

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the html tag data to parse.');
}
$data = $argv[1];

echo "\n\t".'Input : '.$data."\n\n";

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new RuntimeException('You should run composer first.');
}
require $composer;

$parser = new HtmlTagParser();
$node = $parser->parseString($data);
var_dump($node);
