<?php declare(strict_types=1);

/**
 * Test script for html parser.
 * 
 * This file will give the data in object tree form.
 * 
 * Usage:
 * php testhtml.php "<html_document>"
 * 
 * @author Anastaszor
 */

use PhpExtended\Html\HtmlParser;

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the html data to parse.');
}
$data = $argv[1];

echo "\n\t".'Input : '.$data."\n\n";

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new RuntimeException('You should run composer first.');
}
require $composer;

$parser = new HtmlParser();
$node = $parser->parseString($data);
var_dump($node);
