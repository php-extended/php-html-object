<?php declare(strict_types=1);

/**
 * Test script for html parser.
 * 
 * This file will give the data in object tree form.
 * 
 * Usage :
 * php testfile.php "path_or_url"
 * 
 * @author Anastaszor
 */

use PhpExtended\Html\HtmlParser;

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the html data to parse.');
}
$path = $argv[1];

echo "\n\t".'Input : '.$path."\n\n";

$data = file_get_contents($path, false, stream_context_create(['http' => ['ignore_errors' => true]]));

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new RuntimeException('You should run composer first.');
}
require $composer;

$parser = new HtmlParser();
$node = $parser->parseString($data);
var_dump($node);

echo $node->__toString();
