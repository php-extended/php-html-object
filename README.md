# php-extended/php-html-object

A library that implements the php-extended/php-html-interface interface library.

![coverage](https://gitlab.com/php-extended/php-html-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-html-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-html-object ^8`


## Basic Usage

To build a html tree, just use the collection node and the single nodes :

```php

use PhpExtended\Html\HtmlAbstractNode;
use PhpExtended\Html\HtmlCollectionNode;
use PhpExtended\Html\HtmlDoctypeNode;
use PhpExtended\Html\HtmlTextNode;

$document = new HtmlCollectionNode(HtmlAbstractNode::TYPE_DOCUMENT);
$doctype = new HtmlDoctypeNode();
$document->addChild($doctype);
$html = new HtmlCollectionNode('html');
$document->addChild($html);
$body = new HtmlCollectionNode('body');
$html->addChild($body);
$h1 = new HtmlCollectionNode('h1', ['class' => 'title']);
$body->addChild($h1);
$title = new HtmlTextNode('>> Example Title <<');
$h1->addChild($title);
$text = new HtmlCollectionNode('p', [], [new HtmlTextNode('This is an &xample.')]);
$body->addChild($text);

$document->__toString();
// <!DOCTYPE html><html><body><h1 class="title">&gt;&gt; Example Title &lt;&lt;</h1><p>This is an &amp;xample.</p></body></html>

```

To parse html data, do the following :

```php

use PhpExtended\Html\HtmlParser;

$parser = new HtmlParser();
$node = $parser->parse('<!DOCTYPE html><html><head><meta charser="UTF-8" /></head><body><h1>Foo</h1></body></html>');
// $node is not an HtmlCollectionNodeInterface

```


## License

MIT (See [license file](LICENSE)).
